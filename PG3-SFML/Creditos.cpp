#include "Creditos.h"
#include "App.h"
#include <iostream>

Creditos::Creditos()
{
}

Creditos::Creditos(sf::RenderWindow *_window, App *_app):Scene(_window, _app)
{
	Start();
}

Creditos::~Creditos()
{
}

void Creditos::Start()
{
	std::cout << "CREDITOS" << std::endl;
	//font
	if (!font.loadFromFile("assets/font.ttf"))
	{
		std::cout << "Failed to Load Font" << std::endl;
	}
	text.setFont(font);
	text.setCharacterSize(24);
	text.setFillColor(sf::Color::Black);
	text.setStyle(sf::Text::Regular);
	text.setPosition(window->getSize().x / text.getCharacterSize(), window->getSize().y / text.getCharacterSize());

	text.setString("Dev: Nicol�s Pedevilla\nSprites: OpenGameArt.org\nMusic and SFX: incompetech(Kevin MacLeod)\nMade with SFML and POCO\n\nPress BackSpace to go back to the Menu");
}

void Creditos::Update()
{
	while (window->isOpen())
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyPressed:
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace)) 
				{
					app->GoToMenuScene();
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) 
				{
					window->close();
				}
				break;

			case sf::Event::Closed:
				window->close();
				break;
			default:
				break;
			}
		}
		window->clear(sf::Color::White);
		window->draw(text);
		window->display();
	}
}