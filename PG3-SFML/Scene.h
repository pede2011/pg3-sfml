#ifndef SCENE_H
#define SCENE_H
#include "SFML\Graphics.hpp"

class App;

class Scene
{
protected:
	virtual void Start();
	virtual void Update();
	sf::RenderWindow *window;
	App *app;
public:	
	Scene();
	Scene(sf::RenderWindow *_window, App *_app);
	virtual ~Scene();
	void RunScene();
};
#endif // !SCENE_H