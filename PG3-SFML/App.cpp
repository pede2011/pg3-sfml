#include "App.h"
#include "Menu.h"
#include "Juego.h"
#include "Creditos.h"
#include "Scene.h"
#include <Windows.h>

class Menu;
class Juego;
class Creditos;

App::App(float _windowX, float _windowY, std::string _windowName):windowX(_windowX),windowY(_windowY),windowName(_windowName), window(sf::VideoMode(_windowX, _windowY), _windowName)
{
#if defined(_DEBUG)
	std::cout << "Debug Console:" << std::endl;
#else
	ShowWindow(GetConsoleWindow(), SW_HIDE);
	FreeConsole();
#endif

	window.setVerticalSyncEnabled(true);
	window.setFramerateLimit(60);

	castWindow = &window;
	GoToMenuScene();
	//Run();
}

App::~App()
{
}

void App::Run()
{
	while (castWindow->isOpen())
	{
		//FailSafe, just in case
		if (prevScene!= NULL)
		{
			delete prevScene;
			prevScene = NULL;
		}
		currentScene->RunScene();
	}
}


void App::GoToMenuScene() 
{
	std::cout << "Switched to Menu" << std::endl;
	CurrentSceneName = MenuScene;
	castWindow = NULL;
	prevScene = NULL;
	castWindow = &window;
	currentScene = new Menu(castWindow, this);
	Run();
}

void App::GoToGameScene()
{
	std::cout << "Switched to Game" << std::endl;
	CurrentSceneName = GameScene;
	castWindow = NULL;
	prevScene = NULL;
	castWindow = &window;
	currentScene = new Juego(castWindow, this);
	Run();
}
void App::GoToCreditsScene() 
{
	std::cout << "Switched to Credits" << std::endl;
	CurrentSceneName = CreditsScene;
	castWindow = NULL;
	prevScene = NULL;
	castWindow = &window;
	currentScene = new Creditos(castWindow, this);
	Run();
}