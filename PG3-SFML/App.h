#ifndef APP_H
#define APP_H

#include "Scene.h"
#include "Juego.h"
#include "Menu.h"
#include "Creditos.h"

#include <iostream>
class App
{
public:
	App(float _windowX, float _windowY, std::string _windowName);
	~App();
	void Run();
	void GoToMenuScene();
	void GoToGameScene();
	void GoToCreditsScene();
private:
	int CurrentSceneName;	
	Scene *prevScene;
	Scene *currentScene;
	enum Scenes { MenuScene, GameScene, CreditsScene };
	float windowX;
	float windowY;
	std::string windowName;
	sf::RenderWindow window;
	sf::RenderWindow *castWindow = NULL;
};
#endif //APP:H
