#include "Points.h"

bool Points::CheckAllow(int _position)
{
	if (_position >= window->getSize().x - size || _position <= 0 + size)
	{
		return false;
	}

	if (_position >= window->getSize().y - size || _position <= 0 + size)
	{
		return false;
	}

	if (_position <= player->sprite.getGlobalBounds().width + size && _position >= player->sprite.getGlobalBounds().width - size)
	{
		return false;
	}

	if (_position <= player->sprite.getGlobalBounds().height + size && _position >= player->sprite.getGlobalBounds().height - size)
	{
		return false;
	}
	return true;
}

Points::Points()
{
}

Points::Points(std::string _path, float _size, sf::RenderWindow *_window, Player *_player):path(_path),size(_size),window(_window),player(_player)
{
	int tempX = window->getSize().x - size;
	int posX = rand() % tempX - size;

	while (CheckAllow(posX) == false)
	{
		posX = rand() % tempX - size;
	}

	int tempY = window->getSize().y - size;	
	int posY = rand() % tempY - size;
	
	while (CheckAllow(posY) == false)
	{
		posY = rand() % tempY - size;
	}

	if (!texture.loadFromFile(path))
	{
		texture.create(size, size);
	}
	texture.setSmooth(true);
	sprite.setTexture(texture, true);
	sprite.setPosition(posX, posY);
}

Points::~Points()
{
}

void Points::NewPoints()
{
	int tempX = window->getSize().x - size;
	int posX = rand() % tempX - size;

	int tempY = window->getSize().y - size;
	int posY = rand() % tempY - size;

	sprite.setPosition(posX, posY);
}