#include "Enemigo.h"
#include <random>


bool Enemigo::CheckAllow(int _position)
{
	if (_position >= window->getSize().x - size || _position <= 0 + size)
	{
		return false;
	}

	if (_position >= window->getSize().y - size || _position <= 0 + size)
	{
		return false;
	}

	if (_position <= player->sprite.getGlobalBounds().width + size*2 && _position >= player->sprite.getGlobalBounds().width - size*2)
	{
		return false;
	}

	if (_position <= player->sprite.getGlobalBounds().height + size*2 && _position >= player->sprite.getGlobalBounds().height - size*2)
	{
		return false;
	}
	return true;
}

Enemigo::Enemigo(std::string _path, float _size, sf::RenderWindow *_window, Player *_player):path(_path),size(_size),window(_window),player(_player)
{
	int tempX = window->getSize().x - size;
	int posX = rand() % tempX - size;

	while (CheckAllow(posX) == false)
	{
		posX = rand() % tempX - size;
	}

	int tempY = window->getSize().y - size;
	int posY = rand() % tempY - size;

	while (CheckAllow(posY) == false)
	{
		posY = rand() % tempY - size;
	}

	if (!texture.loadFromFile(path))
	{
		texture.create(size, size);
	}
	texture.setSmooth(true);	
	sprite.setTexture(texture, true);
	sprite.setPosition(posX, posY);
}

Enemigo::~Enemigo()
{
}

void Enemigo::Update(Patron *_patron, sf::Sprite *_sprite, sf::RenderWindow *_window)
{
	std::cout << "Moving enemies" << std::endl;
	_patron->execute(_sprite, _window);
}

bool Patron::CheckAllow(sf::Sprite *_sprite, sf::RenderWindow *_window)
{
	sf::Vector2f temp(_sprite->getPosition().x + _sprite->getLocalBounds().width, _sprite->getPosition().y + _sprite->getLocalBounds().height);
	int x = temp.x + movementSpeed;
	int y = temp.y + movementSpeed;
	if (x > _window->getSize().x || x < _sprite->getLocalBounds().width) 
	{
		return false;
	}
	if (y > _window->getSize().y || y < _sprite->getLocalBounds().height) 
	{
		return false;
	}
	return true;
}

void Vertical::execute(sf::Sprite *_sprite, sf::RenderWindow *_window)
{	
	std::cout << "VERTICAL" << std::endl;
	if (CheckAllow(_sprite, _window))
	{
		_sprite->setPosition(_sprite->getPosition().x, _sprite->getPosition().y + movementSpeed);
	}
	else
	{
		movementSpeed *= -1;
	}	
}

void Horizontal::execute(sf::Sprite *_sprite, sf::RenderWindow *_window)
{
	std::cout << "HORIZONTAL" << std::endl;
	if (CheckAllow(_sprite, _window))
	{
		_sprite->setPosition(_sprite->getPosition().x + movementSpeed, _sprite->getPosition().y);
	}
	else
	{
		movementSpeed *= -1;
	}
}