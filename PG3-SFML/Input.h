#ifndef INPUT_H
#define INPUT_H

#include "SFML\Graphics.hpp"
class Input
{
public:
	Input(sf::Sprite &sprite, float speed, sf::RenderWindow *_window);
	~Input();
	void Update();//Call this in the GAME Update
protected:
	bool CheckAllow(int _position);
	float objSpeed;
	sf::RenderWindow *window;
	sf::Sprite &spriteToMove;
};
#endif //INPUT:H
