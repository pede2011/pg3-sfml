#include "Scene.h"
#include "App.h"

Scene::Scene()
{
}

Scene::Scene(sf::RenderWindow *_window, App *_app)
{	
	window = _window;
	app = _app;
}

Scene::~Scene()
{
}

void Scene::Start()
{
}

void Scene::Update()
{
}

void Scene::RunScene()
{
	Update();
}
