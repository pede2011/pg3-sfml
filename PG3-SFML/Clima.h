#ifndef CLIMA_H
#define CLIMA_H
#include<string>

class Clima {
private:
	Clima();
	~Clima();
	static std::string _result;
	static std::string _city;
public:
	static void DoHtmlRequest(const std::string &url);
	static const std::string &GetWeather();
	static const std::string &GetCity();
};
#endif //CLIMA:H
