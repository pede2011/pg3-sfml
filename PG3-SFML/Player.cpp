#include "Player.h"


//class Input;

Player::Player()
{
}

Player::Player(std::string _path, float _sizeX, float _sizeY, sf::RenderWindow *_window, float _speed):path(_path), sizeX(_sizeX),sizeY(_sizeY), window(_window), speed(_speed)
{
	if (!texture.loadFromFile(path))
	{
		texture.create(sizeX, sizeY);
	}
	texture.setSmooth(true);	
	sprite.setTexture(texture, true);
	sprite.setPosition(window->getSize().x/2, window->getSize().y/2);

	input = new Input(sprite, speed, window);
}

Player::~Player()
{
}

void Player::Update()
{
	input->Update();
}