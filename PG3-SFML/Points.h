#ifndef POINTS_H
#define POINTS_

#include "SFML\Graphics.hpp"
#include "Player.h"
#include <iostream>

class Points
{
public:
	Points();
	Points(std::string _path, float _size, sf::RenderWindow *_window, Player *_player);
	~Points();
	void NewPoints();
	sf::Sprite sprite;
private:
	Player *player;
	std::string path;
	float size;
	sf::Texture texture;
	sf::RenderWindow *window;
	bool CheckAllow(int _position);
};
#endif //POINTS_H
