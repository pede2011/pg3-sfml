#include "Clima.h"
#include<iostream>
#include <fstream>

#include<Poco\URI.h>
#include<Poco\Net\HTTPResponse.h>
#include<Poco\Net\HTTPRequest.h>
#include<Poco\Net\HTTPClientSession.h>
#include<Poco\Net\NetException.h>

#include<Poco\JSON\Parser.h>

std::string Clima::_result;
std::string Clima::_city;

Clima::Clima()
{
}

Clima::~Clima()
{
}

void Clima::DoHtmlRequest(const std::string &url) 
{
	//Web
	Poco::URI uri(url);
	std::string temp;
	Poco::Net::HTTPClientSession session(uri.getHost(), uri.getPort());

	try 
	{
		Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_GET, uri.getPathAndQuery(), Poco::Net::HTTPMessage::HTTP_1_1);
		session.sendRequest(req);
	}
	catch (Poco::Net::HostNotFoundException ex) 
	{
		std::cout << ex.what() << std::endl;
		_result = "Unknown";
	}
	catch (Poco::TimeoutException ex) 
	{
		std::cout << ex.what() << std::endl;
		_result = "Unknown";
	}

	try
	{
		Poco::Net::HTTPResponse response;
		std::istream &is = session.receiveResponse(response);
		std::cout << "HTTPResponse Status: " << response.getStatus() << " HTTPResponse Reason: " << response.getReason() << std::endl;

		//Dump into string
		while (std::getline(is, temp))
			std::cout << temp << std::endl;
		//
	}
	catch (Poco::Net::InvalidSocketException ex) 
	{
		std::cout << ex.what() << std::endl;
	}
	
	//JSON

	if (!temp.empty()) 
	{
		Poco::JSON::Parser parser;
		Poco::Dynamic::Var result = parser.parse(temp);
		Poco::JSON::Object::Ptr jsonObj = result.extract<Poco::JSON::Object::Ptr>();
		Poco::Dynamic::Var var = jsonObj->get("query");

		jsonObj = var.extract<Poco::JSON::Object::Ptr>();
		var = jsonObj->get("results");

		jsonObj = var.extract<Poco::JSON::Object::Ptr>();
		var = jsonObj->get("channel");

		jsonObj = var.extract<Poco::JSON::Object::Ptr>();
		var = jsonObj->get("item");

		jsonObj = var.extract<Poco::JSON::Object::Ptr>();
		var = jsonObj->get("condition");

		jsonObj = var.extract<Poco::JSON::Object::Ptr>();
		var = jsonObj->get("text");

		_result = var.convert<std::string>();
		std::cout << "Weather: " << _result << std::endl;		
	}
}

const std::string &Clima::GetWeather() 
{
	return _result;
}

const std::string &Clima::GetCity()
{
	std::ifstream readFile;
	readFile.open("assets/City.txt");
	if (readFile.good() == false)
	{
		std::ofstream createFile("assets/City.txt");//If there isnt a HighScore file for some reason
		createFile << "buenosaires" << std::endl;
		readFile.open("assets/City.txt");
	}
	if (readFile.is_open())
	{
		std::string read;
		std::getline(readFile, _city);
		std::cout << _city << std::endl;
		readFile.close();
	}
	else
	{
		std::cout << "Can't Open File" << std::endl;
	}
	return _city;	
}
