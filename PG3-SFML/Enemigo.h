#ifndef ENEMIGO_H
#define ENEMIGO_H

#include "SFML\Graphics.hpp"
#include "Player.h"
#include <iostream>

class Patron
{
public:
	virtual void execute(sf::Sprite *_sprite, sf::RenderWindow *_window) = 0;
	bool CheckAllow(sf::Sprite *_sprite, sf::RenderWindow *_window);
	float movementSpeed = 15.0f;	
};

class Vertical : public Patron
{
public:
	void execute(sf::Sprite *_sprite, sf::RenderWindow *_window);
};

class Horizontal : public Patron
{
	void execute(sf::Sprite *_sprite, sf::RenderWindow *_window);
};

class Enemigo
{
public:
	Enemigo(std::string _path, float _size, sf::RenderWindow *_window, Player *_player);
	~Enemigo();
	sf::Sprite sprite;
	void Update(Patron *_patron, sf::Sprite *_sprite, sf::RenderWindow *_window);
protected:
	sf::Texture texture;
	float size;
	std::string path;
	sf::RenderWindow *window;
	float posX;
	float posY;
	Patron *patron;
	Player *player;
	bool CheckAllow(int _position);
};
#endif //ENEMIGO:H
