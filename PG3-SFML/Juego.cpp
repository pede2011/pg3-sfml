#include "Juego.h"
#include "App.h"

void SaveScore(int _score)
{
	std::ifstream readFile;
	int value = NULL;
	readFile.open("assets/HighScore.txt");
	if (readFile.is_open())
	{
		std::string read;
		std::getline(readFile, read);
		value = std::stoi(read);
		readFile.close();
	}
	else
	{
		std::ofstream myFile;
		myFile.open("assets/HighScore.txt");
		if (myFile.is_open())
		{
			myFile << _score << std::endl;
			myFile.flush();
			myFile.close();
		}
	}
	if (_score > value)
	{
		std::ofstream myFile;
		myFile.open("assets/HighScore.txt");
		if (myFile.is_open())
		{
			myFile << _score << std::endl;
			myFile.flush();
			myFile.close();
		}
	}
}

Juego::Juego()
{
}

Juego::Juego(sf::RenderWindow *_window, App *_app):Scene(_window,_app)
{	
	Start();
}


Juego::~Juego()
{
}

void Juego::AdditionalInput()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		SaveScore(score);
		window->close();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::BackSpace))
	{
		music.stop();
		app->GoToMenuScene();
	}
}

void Juego::Start()
{	
	std::cout << "JUEGO" << std::endl;
	player = new Player("assets/player.png", playerSize, playerSize, window, playerSpeed);

	enemy1 = new Enemigo("assets/enemy.png", enemySize, window, player);
	enemy2 = new Enemigo("assets/enemy.png", enemySize, window, player);

	point = new Points("assets/coin.png", pointSize, window, player);
	
	timer = 0;

	if (!font.loadFromFile("assets/font.ttf"))
	{
		std::cout << "Failed to load Font" << std::endl;
	}

	lives++;
	
	text.setFont(font);
	text.setCharacterSize(24);
	text.setFillColor(sf::Color::Blue);
	text.setStyle(sf::Text::Regular);
	oss.clear();
	oss.str("");
	oss << "Lives: " << lives << "\nScore: "<<score;
	text.setString(oss.str());

	if (!buffer.loadFromFile("assets/sfx.wav")) 
	{
		std::cout << "Failed to load SFX" << std::endl;
	}
	if (!music.openFromFile("assets/music.ogg")) 
	{
		std::cout << "Failed to load Music" << std::endl;
	}

	sound.setBuffer(buffer);
	music.play();
	music.setLoop(true);
}

void Juego::Update()
{
	sf::Time countdown = update.restart();

	sf::Event event;
	while (window->pollEvent(event))
	{		
		if (event.type == sf::Event::Closed)
		{
			window->close();
		}
	}
	timer += countdown.asSeconds();
	if (timer > 0.1)
	{
		//Colliders Enemies
		if (player->sprite.getGlobalBounds().intersects(enemy1->sprite.getGlobalBounds()) || player->sprite.getGlobalBounds().intersects(enemy2->sprite.getGlobalBounds()))
		{
			lives--;
			player->sprite.setPosition(window->getSize().x / 2, window->getSize().y / 2);
			oss.clear();
			oss.str("");
			oss << "Lives: " << lives << "\nScore: " << score;
			text.setString(oss.str());
			if (lives <= 0)
			{
				SaveScore(score);
				app->GoToMenuScene();
			}
		}
		//Colliders Points
		if (player->sprite.getGlobalBounds().intersects(point->sprite.getGlobalBounds()))
		{
			score++;
			sound.play();
			oss.clear();
			oss.str("");
			oss << "Lives: " << lives << "\nScore: " << score;
			text.setString(oss.str());
			point->NewPoints();
		}

		player->Update();

		if (score%2 == 0)
		{
			enemy1->Update(&patronEnemyV, &enemy1->sprite, window);
			enemy2->Update(&patronEnemyH, &enemy2->sprite, window);
		}
		else
		{
			enemy1->Update(&patronEnemyH, &enemy1->sprite, window);
			enemy2->Update(&patronEnemyV, &enemy2->sprite, window);
		}

		AdditionalInput();		
		timer = 0;		
	}	

	window->clear(sf::Color::Black);
	window->draw(text);
	window->draw(player->sprite);
	window->draw(enemy1->sprite);
	window->draw(enemy2->sprite);
	window->draw(point->sprite);
	window->display();
}
