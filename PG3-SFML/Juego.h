#ifndef JUEGO_H
#define JUEGO_H

#include "SFML/Graphics.hpp"
#include "SFML\Audio\Music.hpp""
#include "SFML\Audio.hpp"
#include "Scene.h"
#include "Enemigo.h"
#include "Player.h"
#include "Points.h"
#include <iostream>
#include <fstream>
#include <sstream>

class Juego : public Scene
{
public:
	Juego();
	Juego(sf::RenderWindow *_window, App *_app);
	~Juego() override;	
private:
	void Start() override;
	void Update() override;

	Player *player;
	Enemigo *enemy1;
	Enemigo *enemy2;
	Vertical patronEnemyV;
	Horizontal patronEnemyH;
	Points *point;

	int score = 0;
	int lives = 3;
	float playerSize = 10.0f;
	float playerSpeed = 10.0f;
	float enemySize = 10.0f;
	float pointSize = 10.0f;
	std::ostringstream oss;
	float timer;


	void AdditionalInput();	

	sf::Clock update;
	sf::Text text;
	sf::Font font;
	sf::Music music;
	sf::Sound sound;
	sf::SoundBuffer buffer;
};
#endif //JUEGO_H
