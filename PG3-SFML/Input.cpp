#include "Input.h"
#include <iostream>
class Source;

Input::Input(sf::Sprite &sprite, float speed, sf::RenderWindow *_window):spriteToMove(sprite),objSpeed(speed),window(_window)
{
}

Input::~Input()
{
}

bool Input::CheckAllow(int _position)
{
	if (_position >= window->getSize().x - spriteToMove.getGlobalBounds().width || _position <= 0 + spriteToMove.getGlobalBounds().width)
	{
		return false;
	}
	
	if (_position >= window->getSize().y - spriteToMove.getGlobalBounds().height || _position <= 0 + spriteToMove.getGlobalBounds().height)
	{
		return false;
	}
	
	return true;
	
}

void Input::Update()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		spriteToMove.move(-objSpeed, 0);
		/*if (CheckAllow(spriteToMove.getGlobalBounds().width + objSpeed))
		{
			std::cout << "Moving PLAYER Left" << std::endl;
			spriteToMove.move(-objSpeed, 0);
		}
		else
		{
			return;
		}*/
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		spriteToMove.move(objSpeed, 0);
		/*if (CheckAllow(spriteToMove.getGlobalBounds().width - objSpeed))
		{
			std::cout << "Moving PLAYER Right" << std::endl;
			spriteToMove.move(objSpeed, 0);
		}
		else
		{
			return;
		}*/
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		spriteToMove.move(0, -objSpeed);
		/*if (CheckAllow(spriteToMove.getGlobalBounds().height + objSpeed))
		{
			std::cout << "Moving PLAYER Up" << std::endl;
			spriteToMove.move(0, -objSpeed);
		}
		else
		{
			return;
		}*/
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		spriteToMove.move(0, objSpeed);
		/*if (CheckAllow(spriteToMove.getGlobalBounds().height + objSpeed))
		{
			std::cout << "Moving PLAYER Down" << std::endl;
			spriteToMove.move(0, objSpeed);
		}
		else
		{
			return;
		}*/
	}	
}	
