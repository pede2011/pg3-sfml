#ifndef CREDITOS_H
#define CREDITOS_H

#include "Scene.h"
class Creditos : public Scene
{
public:
	Creditos();
	Creditos(sf::RenderWindow *_window, App *_app);
	~Creditos();
private:
	void Start() override;
	void Update() override;
	sf::Text text;
	sf::Font font;
};
#endif //CREDITOS_H
