#ifndef MENU_H
#define MENU_H

#include "SFML\Graphics.hpp"
#include "Scene.h"
class Menu : public Scene
{
public:
	Menu();
	Menu(sf::RenderWindow *_window, App *_app);
	~Menu() override;
private:
	void Start() override;
	void Update() override;
	sf::Text text;
	sf::Font font;
};
#endif //MENU:H
