#include "Menu.h"
#include "App.h"
#include "Clima.h"
#include <iostream>
#include <fstream>

std::string ReadHighScore()
{
	std::string read;
	std::ifstream readFile;	
	readFile.open("assets/HighScore.txt");
	if (readFile.good()==false)
	{
		std::ofstream createFile("assets/HighScore.txt");//If there isnt a HighScore file for some reason
		createFile << "0" << std::endl;
		readFile.open("assets/HighScore.txt");
	}
	if (readFile.is_open())
	{
		std::getline(readFile, read);
		std::cout << read << std::endl;
		readFile.close();
		if (read=="")//If there isnt a score yet
		{
			read = "0";
		}
	}
	else
	{
		std::cout << "Can't Open File" << std::endl;
		read = "UNKNOWN";
	}

	std::string toReturn = "Your current HighScore is " + read;

	return toReturn;
}

std::string GetWeather()
{
	Clima::DoHtmlRequest("http://query.yahooapis.com/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D\""+Clima::GetCity()+"\")&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");

	std::string clima = Clima::GetWeather();

	std::string ciudad = Clima::GetCity();

	std::cout << "The weather in "<<ciudad<<" is "<<clima<< std::endl;

	std::string toReturn = "The weather in " + ciudad + " is " + clima;

	return toReturn;
}

Menu::Menu()
{
}

Menu::Menu(sf::RenderWindow *_window, App *_app):Scene(_window,_app)
{
	Start();
}

Menu::~Menu()
{
}

void Menu::Start()
{	
	if (!font.loadFromFile("assets/font.ttf"))
	{
		std::cout << "Failed to load FONT.TFF" << std::endl;
	}
	text.setFont(font);
	text.setCharacterSize(24);
	text.setFillColor(sf::Color::Black);
	text.setStyle(sf::Text::Regular);
	text.setPosition(window->getSize().x / text.getCharacterSize(), window->getSize().y / text.getCharacterSize());

	std::cout << "MENU" << std::endl;	
	std::cout<<ReadHighScore()<<std::endl;
	std::cout<<GetWeather()<<std::endl;

	text.setString("Hello!\nPress TAB to Play\nPress C to see the Credits\nPress ESC to Exit\nPress BackSlash to return to Menu\n\n"+ReadHighScore() + "\n" + GetWeather());	
}

void Menu::Update()
{
	while (window->isOpen())
	{
		sf::Event event;
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::KeyPressed:
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Tab))
				{
					app->GoToGameScene();
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::C)) {
					app->GoToCreditsScene();
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
					window->close();
				}
				break;

			case sf::Event::Closed:				
				window->close();
				break;

			default:
				break;
			}
		}


		window->clear(sf::Color::White);
		window->draw(text);
		window->display();
	}	
}