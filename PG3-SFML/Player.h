#ifndef PLAYER_H
#define PLAYER_H

#include "SFML\Graphics.hpp"
#include "Input.h"
//#include <stdio.h>
class Player
{
public:
	Player();
	Player(std::string _path, float _sizeX, float _sizeY, sf::RenderWindow *_window, float _speed);
	~Player();
	sf::Sprite sprite;
	void Update();
protected:
	sf::RenderWindow *window;
	std::string path;
	float sizeX, sizeY, speed;
	sf::Texture texture;
	Input* input;
};
#endif //PLAYER:H
